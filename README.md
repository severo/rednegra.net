# Instalación

Instalación del framework UIKit.

```
nvm install 8
yarn add uikit
```

## Compilación

Compilación del framework UIKit:

```
cd node_modules/uikit/
yarn install
yarn run compìle
```

## Customización del CSS

Usamos [https://getuikit.com/docs/less](https://getuikit.com/docs/less). Para compilar a cada cambio:

```
yarn run watch
```

## Uso de less

```
yarn add less --save-dev
```
